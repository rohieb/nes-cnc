#ifndef __IO_H__
#define __IO_H__

typedef const unsigned int iomem;

// first, some bit helpers:
inline void setbit(iomem addr, const uint8_t bit) {
	*(volatile uint8_t *)(addr) |=  (1 << (bit));
}
inline void clrbit(iomem addr, const uint8_t bit) {
	*(volatile uint8_t *)(addr) &= ~(1 << (bit));
}
inline void setclrbit(iomem addr, const uint8_t bit, const bool value) {
	if (value) {
		setbit(addr, bit);
	} else {
		clrbit(addr, bit);
	}
}

// read and write macros
inline uint8_t readb(iomem addr) {
	return *(volatile uint8_t *)(addr);
}
inline void writeb(uint8_t val, iomem addr) {
	*(volatile uint8_t *)(addr) = val;
}

#endif  // __IO_H__

#define F_CPU 16000000UL

#include <util/delay.h>
#include "gpio.h"

static const struct gpio button = GPIO(A, 1);
static const struct gpio led    = GPIO(B, 1);

void setup() {
	gpio_direction(button, Input);
	gpio_pupd(button, PullDown);

	gpio_direction(led, Output);
	gpio_write(led, High);
}

void loop() {
	_delay_ms(1000);
	gpio_toggle(led);
}

int main() {
	setup();
	while (1) {
		loop();
	}
	return 0;
}

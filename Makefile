#!/usr/bin/env make -f

CROSS_PREFIX=avr-
MCU=attiny167

CC=$(CROSS_PREFIX)gcc
CFLAGS=-Wall -g -Os -mmcu=$(MCU) -x c++ -std=c++11
LDFLAGS=-Wall -g -Os -mmcu=$(MCU) -Wl,--gc-sections
OBJCOPY=$(CROSS_PREFIX)objcopy
OBJDUMP=$(CROSS_PREFIX)objdump
SIZE=$(CROSS_PREFIX)size

.SECONDARY:

all: firmware.hex
asm: firmware.bin.S firmware.S

flash: firmware.hex
	micronucleus --flash $<

run: firmware.hex
	micronucleus --run $<

clean:
	rm -f firmware.o firmware.bin firmware.hex firmare.S firmware.bin.S

firmware.o: firmware.c *.h

firmware.bin: firmware.o
	$(CC) $(LDFLAGS) -o $@ $<

%.hex: %.bin
	$(OBJCOPY) -j .text -j .text.startup -j .data -O ihex $< $@
	$(SIZE) $@

%.S: %.o
	$(OBJDUMP) -d $< > $@

%.bin.S: %.bin
	$(OBJDUMP) -d $< > $@

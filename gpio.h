#ifndef __GPIO_H__
#define __GPIO_H__

#include "io.h"

// directly from the datasheet:
static iomem PINA	= 0x20;
static iomem DDRA	= 0x21;
static iomem PORTA	= 0x22;

static iomem PINB	= 0x23;
static iomem DDRB	= 0x24;
static iomem PORTB	= 0x25;

struct gpio {
	const uint8_t pin;
	iomem ddr_reg;
	iomem port_reg;
	iomem pin_reg;
};

#define GPIO(port, pin_number) \
	{ \
		.pin = pin_number, \
		.ddr_reg  = DDR##port, \
		.port_reg = PORT##port, \
		.pin_reg  = PIN##port, \
	}

// then, to the pin helper functions:
enum PinMode {
	Input = 0,
	Output = 1,
};
inline void gpio_direction(const struct gpio g, PinMode mode) {
	setclrbit(g.ddr_reg, g.pin, mode);
}

enum PinPull { 
	PullDown = 0,
	PullUp = 1,
};
inline void gpio_pupd(const struct gpio g, PinPull mode) {
	setclrbit(g.port_reg, g.pin, mode);
}

enum PinValue {
	High = 1,
	Low = 0,
};
inline void gpio_write(const struct gpio g, PinValue value) {
	setclrbit(g.port_reg, g.pin, value);
}

inline void gpio_toggle(const struct gpio g) {
	setbit(g.pin_reg, g.pin);
}

inline int gpio_read(const struct gpio g) {
	return (readb(g.port_reg) >> g.pin) & (1 << g.pin);
}

#endif // __GPIO_H__
